import unittest
import random
from parameterized import parameterized

from algorithms.bubble_sort import BubbleSort
from algorithms.merge_sort import MergeSort
from algorithms.quick_sort import QuickSort

algorithms = [[BubbleSort()], [MergeSort()], [QuickSort()]]
threads_num = [2, 3, 4, 5, 6, 7, 8, 9]

algorithms_threads = [[algorithms[i][0], threads_num[j]] for i in range(len(algorithms)) for j in
                      range(len(threads_num))]


class AlgorithmTest(unittest.TestCase):

    @parameterized.expand(algorithms)
    def test_sequential(self, algorithm):
        arr = generate_random_list(100)
        sorted = arr.copy()
        sorted.sort()
        arr = algorithm.sort(1, arr)
        self.assertListEqual(sorted, arr)

    @parameterized.expand(algorithms_threads)
    def test_parallel(self, algorithm, threads_num):
        arr = generate_random_list(20)
        sorted = arr.copy()
        sorted.sort()
        arr = algorithm.sort(threads_num, arr)
        self.assertListEqual(sorted, arr)


def generate_random_list(size):
    return [(random.randint(0, 10000)) for i in range(size)]
