
import matplotlib.pyplot as plt


def plot(name, threads_times, sizes):
    '''
    Creates and save plot
    :param name: plot name and file name
    :param threads_times: thread number ->  times array
    :param sizes: array of problem sizes
    :return: none
    '''
    print(threads_times)
    print(sizes)
    for for_thread_number in threads_times.keys():
        plt.plot(sizes, threads_times[for_thread_number], 'o-', label=for_thread_number)
    plt.title(name)
    plt.legend(title="Liczba wątków:")
    plt.xlabel("Rozmiar problemu")
    plt.ylabel("Czas [s]")
    plt.savefig(name + '.pdf')
    plt.close()
