import random
import time

import plot
from algorithms.bubble_sort import BubbleSort
from algorithms.merge_sort import MergeSort
from algorithms.quick_sort import QuickSort


def rand(numbers):
    arr = []
    for i in range(numbers):
        arr.append(round(random.uniform(-10000000.0, 10000000.0), 2))
    return arr


def menu():
    arr = []
    choice = 1
    number_of_threads = 1
    q = QuickSort()
    b = BubbleSort()
    m = MergeSort()

    while choice != 0:
        print("\n1 - wylosuj liczby do posortowania")
        print("2 - wczytaj liczby z klawiatury")
        print("3 - podaj liczbę wątków")
        print("4 - wyświetl aktualne parametry")
        print("5 - bubble sort")
        print("6 - merge sort")
        print("7 - quick sort")
        print("8 - testy automatyczne")
        print("9 - wczytaj dane z pliku")
        print("0 - wyjście")
        choice = int(input("\nPodaj numer\n"))

        match choice:
            case 1:
                length = int(input("Podaj liczbę liczb:\n"))
                arr = rand(length)
                print("Wylosowane liczby: ")
                print(arr)

            case 2:
                length = int(input("Podaj liczbę liczb: "))
                arr.clear()
                while length > 0:
                    arr.append(input())
                    length -= 1
                print("Podane liczby: ")
                print(arr)

            case 3:
                number_of_threads = int(input("Podaj liczbę wątków: "))

                if number_of_threads < 1:
                    print("Liczba wątków musi być dodatnia.")
                    number_of_threads = 1

            case 4:
                print("Liczby do posortowania:")
                print(arr)
                print("Liczba wątków:")
                print(number_of_threads)

            case 5:
                if len(arr) == 0:
                    print("Nie wybrano liczb do posortowania.")
                else:
                    result = b.sort(number_of_threads, arr)
                    print("Wynik:")
                    print(result)

            case 6:
                if len(arr) == 0:
                    print("Nie wybrano liczb do posortowania")
                else:
                    result = m.sort(number_of_threads, arr)
                    print("Wynik:")
                    print(result)

            case 7:
                if len(arr) == 0:
                    print("Nie wybrano liczb do posortowania")
                else:

                    result = q.sort(number_of_threads, arr)
                    print("Wynik:")
                    print(result)

            case 8:
                bubble_size = [10000, 15000, 20000, 25000]
                merge_size = [1000000, 1500000, 2000000, 2500000]
                quick_size = [1000000, 1500000, 2000000, 2500000]
                sizes = [bubble_size, merge_size, quick_size]

                threads = [1, 2, 3, 4, 5, 6]
                algorithms = [b, m, q]

                repeats = 5

                for algorithm in algorithms:  # dla każdego algorytmu stworzenie nowego pliku
                    file = open(algorithm.get_name() + ".txt", "w")
                    file.close()


                for algorithm in algorithms:  # dla każdego algorytmu
                    print(f"algorytm: {algorithm.get_name()}")

                    measurements = {}

                    for thread in threads:  # dla każdej liczby wątków
                        print(f"liczba wątków: {thread}")

                        times = []

                        for size in sizes[algorithms.index(algorithm)]:  # dla każdej wielkości tablicy
                            print(f"rozmiar: {size}")
                            arr = rand(size)
                            result = 0

                            for i in range(repeats):
                                print(f"powtórzenie nr: {i+1}")
                                start = time.time()
                                algorithm.sort(thread, arr.copy())
                                end = time.time()
                                result += (end - start)
                                print("czas: " + str(end - start))

                            result = result / repeats
                            times.append(result)
                            file = open(algorithm.get_name() + ".txt", "a")
                            file.write("rozmiar tablicy: " + str(size) + "; liczba watkow: " + str(thread) + " - " + str(
                                result) + "s\n")
                            print(algorithm.get_name() + ": rozmiar tablicy: " + str(size) + "; liczba watkow: " + str(thread) + " - " + str(
                                result) + "s")
                            file.close()

                        measurements[thread] = times
                    plot.plot(algorithm.get_name(), measurements, sizes[algorithms.index(algorithm)])

                print("\n\nKoniec pomiarów\n\n")
                break
            case 9:
                path = input("Podaj ścieżkę: ")
                try:
                    with open(path, 'r') as file:
                        arr = [int(number) for number in file.read().split(',')]

                except OSError as e:
                    print("Problem z plikiem.")

if __name__ == '__main__':
    menu()
