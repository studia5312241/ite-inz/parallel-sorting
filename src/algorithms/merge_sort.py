import math
import threading
import multiprocessing

from algorithms.algorithm_abstract import AlgorithmAbstract, List, Num


class MergeSort(AlgorithmAbstract):

    def __init__(self):
        self._lock = threading.Lock()

    def get_name(self):
        return "MergeSort"

    def sort(self, threads_num: int, arr: List[Num]) -> List[Num]:
        if threads_num == 1:
            return merge_sort(arr)
        elif threads_num > 1:
            return merge_sort_parallel(arr, threads_num)
        else:
            raise ValueError("Number of threads must be positive")

def merge(*args):
    left, right = args[0] if len(args) == 1 else args
    left_length, right_length = len(left), len(right)
    left_index, right_index = 0, 0
    merged = []
    while left_index < left_length and right_index < right_length:
        if left[left_index] <= right[right_index]:
            merged.append(left[left_index])
            left_index += 1
        else:
            merged.append(right[right_index])
            right_index += 1
    if left_index == left_length:
        merged.extend(right[right_index:])
    else:
        merged.extend(left[left_index:])
    return merged

def merge_sort(data):
    length = len(data)
    if length <= 1:
        return data
    middle = length // 2
    left = merge_sort(data[:middle])
    right = merge_sort(data[middle:])
    return merge(left, right)

def merge_sort_parallel(data, threads_num):
    pool = multiprocessing.Pool(processes=threads_num)
    size = int(math.ceil(float(len(data)) / threads_num))
    data = [data[i * size:(i + 1) * size] for i in range(threads_num)]
    data = pool.map(merge_sort, data)
    while len(data) > 1:
        extra = data.pop() if len(data) % 2 == 1 else None
        data = [(data[i], data[i + 1]) for i in range(0, len(data), 2)]
        data = pool.map(merge, data) + ([extra] if extra else [])
    return data[0]
