from abc import ABC, abstractmethod
from typing import List, Union

Num = Union[int, float]

class AlgorithmAbstract(ABC):

    @abstractmethod
    def sort(self, threads_num: int, arr: List[Num]) -> List[Num]:
        pass

    def _split_arr(self, arr: List[Num], threads_num: int) -> List[Num]:
        biggest_item = max(arr)
        lists = [[] for _ in range(threads_num)]
        split_factor = biggest_item // threads_num

        for j in range(1, len(lists)):
            for i in arr:
                if i <= (split_factor * j):
                    lists[j - 1].append(i)
                    arr = [x for x in arr if x != i]
            lists[-1] = arr

        return lists
