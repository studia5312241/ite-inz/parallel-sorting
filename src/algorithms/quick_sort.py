import multiprocessing
from algorithms.algorithm_abstract import AlgorithmAbstract, List, Num


class QuickSort(AlgorithmAbstract):

    def get_name(self):
        return "QuickSort"

    def sort(self, threads_num: int, arr: List[Num]) -> List[Num]:
        if threads_num == 1:
            serial_quicksort(arr)
            return arr
        elif threads_num > 1:
            arr = self._parallel_sort(threads_num, arr)
            return arr
        else:
            raise ValueError("Number of threads must be positive")

    def _parallel_sort(self, threads_num: int, arr: List[Num]) -> List[Num]:
        receive, send = multiprocessing.Pipe(duplex=False)
        process = multiprocessing.Process(target=parallel_quicksort, args=(arr, send, 1, threads_num))
        process.start()
        result = receive.recv()
        process.join()
        return result


def parallel_quicksort(arr, send, process_number,
                       max_process_number):
    if len(arr) > 0:
        if process_number >= max_process_number:
            serial_quicksort(arr)
            send.send(arr)
            send.close()
        else:
            pivot = arr.pop((len(arr) - 1) // 2)
            smaller = []
            larger = []
            partition(arr, smaller, larger, pivot)
            receive_smaller, send_smaller = multiprocessing.Pipe(duplex=False)
            receive_larger, send_larger = multiprocessing.Pipe(duplex=False)
            new_process_count = 2 * process_number + 1
            smaller_process = multiprocessing.Process(target=parallel_quicksort, args=(smaller, send_smaller, new_process_count, max_process_number))
            larger_process = multiprocessing.Process(target=parallel_quicksort, args=(larger, send_larger, new_process_count, max_process_number))
            smaller_process.start()
            larger_process.start()
            smaller = receive_smaller.recv()
            larger = receive_larger.recv()
            arr.extend(smaller)
            arr.append(pivot)
            arr.extend(larger)
            send.send(arr)
            send.close()
            smaller_process.join()
            larger_process.join()
            smaller_process.close()
            larger_process.close()
    else:
        send.send(arr)
        send.close()


def serial_quicksort(arr):
    if (len(arr) > 0):
        partitioning_element = arr.pop((len(arr) - 1) // 2)
        smaller = []
        larger = []
        partition(arr, smaller, larger, partitioning_element)
        serial_quicksort(smaller)
        serial_quicksort(larger)
        arr.extend(smaller)
        arr.append(partitioning_element)
        arr.extend(larger)


def partition(arr, smaller, larger, pivot):
    smaller.clear()
    larger.clear()

    while (len(arr) > 0):
        element = arr.pop()
        if (element > pivot):
            larger.append(element)
        else:
            smaller.append(element)
