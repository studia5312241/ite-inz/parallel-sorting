from algorithms.algorithm_abstract import AlgorithmAbstract, Num, List
import threading
import itertools


class BubbleSort(AlgorithmAbstract):

    def __init__(self):
        self._lock = threading.Lock()

    def get_name(self):
        return "BubbleSort"

    def sort(self, threads_num: int, arr: List[Num]) -> List[Num]:
        if threads_num == 1:
            return _sequential_sort(arr)
        elif threads_num > 1:
            return self._parallel_sort(threads_num, arr)
        else:
            raise ValueError("Number of threads must be positive")

    def _parallel_sort(self, threads_num: int, arr: List[Num]) -> List[Num]:

        lists = self._split_arr(arr, threads_num)

        active_threads = []
        for list_item in lists:
            t = threading.Thread(target=self._parallel_single_sort, args=(list_item,))
            t.start()
            active_threads.append(t)

        for thread in active_threads:
            thread.join()

        return list(itertools.chain(*lists))

    def _parallel_single_sort(self, arr):
        self._lock.acquire()
        _sequential_sort(arr)
        self._lock.release()


def _sequential_sort(arr: List[Num]) -> List[Num]:
    n = len(arr)
    swapped = False
    for i in range(n - 1):
        for j in range(0, n - i - 1):
            if arr[j] > arr[j + 1]:
                swapped = True
                arr[j], arr[j + 1] = arr[j + 1], arr[j]
        if not swapped:
            return arr
    return arr
